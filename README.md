Target Communication Framework
==============================
[TCF] is a vendor-neutral, lightweight, extensible network protocol
mainly for communicating with embedded systems (targets). On top of the
protocol specification, which is the core of TCF, the project includes
a plain-C implementation of a lightweight extendable target agent, Java
client API (usable stand-alone or on top of Eclipse), [Python] and
[Lua] client APIs, complete debugger UI implementation in Eclipse,
integration with [CDT], Target Explorer, documentation and usage examples.

Contents
--------

[agent](agent)
: The reference agent, implementing debug related [services] e.g.
Memory, RunControl, Registers, Stacktrace, Breakpoints, Symbols, LineNumbers,
Expressions.

[docker](docker/README.md)
: Dockerfile image for host and cross builds of the debug agent.

[examples](agent)
: Implementation of a daytime service.

[server](server)
: A proxy server sitting between the client on the host and an agent
running/embedded on the target as part of the firmware or RTOS. It
implements Symbols and LineNumbers [services].

[tests](tests)
: Standalone tools for testing (e.g dwarf, extended commandline client)

Links
-----

  * [Getting started](https://download.eclipse.org/tools/tcf/tcf-docs/TCF%20Getting%20Started.html)
  * [TCF Specification](https://download.eclipse.org/tools/tcf/tcf-docs/TCF%20Specification.html)
  * [Mailinglist](https://dev.eclipse.org/mailman/listinfo/tcf-dev)
  * [Bugzilla](https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced;classification=Tools;product=TCF)

[TCF]:      https://wiki.eclipse.org/TCF (Landing page)
[Python]:   https://git.eclipse.org/c/tcf/org.eclipse.tcf.git/tree/python (Python client repository)
[Lua]:      https://download.eclipse.org/tools/tcf/tcf-docs/TCF%20Lua%20Integration.html (Lua guide)
[CDT]:      https://git.eclipse.org/c/tcf/org.eclipse.tcf.git (CDT/Java repository)
[services]: https://download.eclipse.org/tools/tcf/tcf-docs/TCF%20Services.html
