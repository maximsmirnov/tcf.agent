/*******************************************************************************
 * Copyright (c) 2020-2024 Xilinx, Inc. and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 * The Eclipse Public License is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 * http://www.eclipse.org/org/documents/edl-v10.php.
 * You may elect to redistribute this code under either of these licenses.
 *
 * Contributors:
 *     Xilinx - initial API and implementation
 *******************************************************************************/

#ifndef D_cpu_regs_gdb_riscv64
#define D_cpu_regs_gdb_riscv64

#include <tcf/config.h>

static const char * cpu_regs_gdb_riscv64 =
"<architecture>riscv:rv64</architecture>\n"
"<feature name='org.gnu.gdb.riscv.cpu'>\n"
"  <reg name='zero' bitsize='64' type='int' id='0' />\n"
"  <reg name='ra'   bitsize='64' type='code_ptr' id='1' />\n"
"  <reg name='sp'   bitsize='64' type='data_ptr' id='2' />\n"
"  <reg name='gp'   bitsize='64' type='data_ptr' id='3' />\n"
"  <reg name='tp'   bitsize='64' type='data_ptr' id='4' />\n"
"  <reg name='t0'   bitsize='64' type='int' id='5' />\n"
"  <reg name='t1'   bitsize='64' type='int' id='6' />\n"
"  <reg name='t2'   bitsize='64' type='int' id='7' />\n"
"  <reg name='fp'   bitsize='64' type='data_ptr' id='8' />\n"
"  <reg name='s1'   bitsize='64' type='int' id='9' />\n"
"  <reg name='a0'   bitsize='64' type='int' id='10' />\n"
"  <reg name='a1'   bitsize='64' type='int' id='11' />\n"
"  <reg name='a2'   bitsize='64' type='int' id='12' />\n"
"  <reg name='a3'   bitsize='64' type='int' id='13' />\n"
"  <reg name='a4'   bitsize='64' type='int' id='14' />\n"
"  <reg name='a5'   bitsize='64' type='int' id='15' />\n"
"  <reg name='a6'   bitsize='64' type='int' id='16' />\n"
"  <reg name='a7'   bitsize='64' type='int' id='17' />\n"
"  <reg name='s2'   bitsize='64' type='int' id='18' />\n"
"  <reg name='s3'   bitsize='64' type='int' id='19' />\n"
"  <reg name='s4'   bitsize='64' type='int' id='20' />\n"
"  <reg name='s5'   bitsize='64' type='int' id='21' />\n"
"  <reg name='s6'   bitsize='64' type='int' id='22' />\n"
"  <reg name='s7'   bitsize='64' type='int' id='23' />\n"
"  <reg name='s8'   bitsize='64' type='int' id='24' />\n"
"  <reg name='s9'   bitsize='64' type='int' id='25' />\n"
"  <reg name='s10'  bitsize='64' type='int' id='26' />\n"
"  <reg name='s11'  bitsize='64' type='int' id='27' />\n"
"  <reg name='t3'   bitsize='64' type='int' id='28' />\n"
"  <reg name='t4'   bitsize='64' type='int' id='29' />\n"
"  <reg name='t5'   bitsize='64' type='int' id='30' />\n"
"  <reg name='t6'   bitsize='64' type='int' id='31' />\n"
"  <reg name='pc'   bitsize='64' type='code_ptr' id='6065' />\n"
"</feature>\n"
"<feature name='org.gnu.gdb.riscv.fpu'>\n"
"<union id='riscv_double'>\n"
"  <field name='float' type='ieee_single' />\n"
"  <field name='double' type='ieee_double' />\n"
"</union>\n"
"  <reg name='ft0' bitsize='64' type='riscv_double' id='32' />\n"
"  <reg name='ft1' bitsize='64' type='riscv_double' id='33' />\n"
"  <reg name='ft2' bitsize='64' type='riscv_double' id='34' />\n"
"  <reg name='ft3' bitsize='64' type='riscv_double' id='35' />\n"
"  <reg name='ft4' bitsize='64' type='riscv_double' id='36' />\n"
"  <reg name='ft5' bitsize='64' type='riscv_double' id='37' />\n"
"  <reg name='ft6' bitsize='64' type='riscv_double' id='38' />\n"
"  <reg name='ft7' bitsize='64' type='riscv_double' id='39' />\n"
"  <reg name='fs0' bitsize='64' type='riscv_double' id='40' />\n"
"  <reg name='fs1' bitsize='64' type='riscv_double' id='41' />\n"
"  <reg name='fa0' bitsize='64' type='riscv_double' id='42' />\n"
"  <reg name='fa1' bitsize='64' type='riscv_double' id='43' />\n"
"  <reg name='fa2' bitsize='64' type='riscv_double' id='44' />\n"
"  <reg name='fa3' bitsize='64' type='riscv_double' id='45' />\n"
"  <reg name='fa4' bitsize='64' type='riscv_double' id='46' />\n"
"  <reg name='fa5' bitsize='64' type='riscv_double' id='47' />\n"
"  <reg name='fa6' bitsize='64' type='riscv_double' id='48' />\n"
"  <reg name='fa7' bitsize='64' type='riscv_double' id='49' />\n"
"  <reg name='fs2' bitsize='64' type='riscv_double' id='50' />\n"
"  <reg name='fs3' bitsize='64' type='riscv_double' id='51' />\n"
"  <reg name='fs4' bitsize='64' type='riscv_double' id='52' />\n"
"  <reg name='fs5' bitsize='64' type='riscv_double' id='53' />\n"
"  <reg name='fs6' bitsize='64' type='riscv_double' id='54' />\n"
"  <reg name='fs7' bitsize='64' type='riscv_double' id='55' />\n"
"  <reg name='fs8' bitsize='64' type='riscv_double' id='56' />\n"
"  <reg name='fs9' bitsize='64' type='riscv_double' id='57' />\n"
"  <reg name='fs10' bitsize='64' type='riscv_double' id='58' />\n"
"  <reg name='fs11' bitsize='64' type='riscv_double' id='59' />\n"
"  <reg name='ft8' bitsize='64' type='riscv_double' id='60' />\n"
"  <reg name='ft9' bitsize='64' type='riscv_double' id='61' />\n"
"  <reg name='ft10' bitsize='64' type='riscv_double' id='62' />\n"
"  <reg name='ft11' bitsize='64' type='riscv_double' id='63' />\n"
"  <reg name='fflags' bitsize='32' type='int' id='4097' />\n"
"  <reg name='frm' bitsize='32' type='int' id='4098' />\n"
"  <reg name='fcsr' bitsize='32' type='int' id='4099' />\n"
"</feature>\n"
"<feature name='org.gnu.gdb.riscv.csr'>\n"
"</feature>\n";

#endif /* D_cpu_regs_gdb_riscv64 */
